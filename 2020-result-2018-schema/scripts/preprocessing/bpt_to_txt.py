"""
Script for extracting text (line-by-line) from BPT XML files
(needed for the CGN corpus)

Requires BeautifulSoup 4
"""

import bs4
import sys

if len(sys.argv) != 2:
    print("Usage: python bpt_to_txt.py [BPT file]")

with open(sys.argv[1]) as f:
    soup = bs4.BeautifulSoup(f.read(), "lxml")
    for utterance in soup.find_all("fau"):
        text = []
        for word in utterance.findChildren():
            text.append(word["w"])
        print(" ".join(text))
