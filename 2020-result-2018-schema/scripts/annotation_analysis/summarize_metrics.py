"""
Script for producing LaTeX tables and plotting bar charts for agreement metrics
"""

import os
import json

import numpy as np
from tabulate import tabulate
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors


def create_latex_table(dirs, file_path, metrics_to_include=None):
    """
    From json files (saved in dirs) creates a summary table in latex format in the specified file_path
    :param dirs: list of directory paths
    :param file_path: path where the output file should be
    :param metrics_to_include: List of strings of the metrics to be included in the tables
    :return: latex file
    """

    folders = sorted([folder for folder in os.listdir(dirs[0]) if os.path.isdir(os.path.join(dirs[0], folder))])
    metrics_file = 'metrics.json'

    # Empty file
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write('')

    for dir in dirs:
        metrics = {}
        rows = {}
        columns = []
        for folder in folders:
            with open(os.path.join(dir, folder, metrics_file)) as f:
                metrics[folder] = json.load(f)

        with open(file_path, 'a', encoding='utf-8') as f:
            headers = {key:idx for idx, key in enumerate(sorted(metrics.keys()))}

            for folder in sorted(metrics.keys()):
                for layer in sorted(metrics[folder].keys()):
                    if layer not in rows:
                        rows[layer] = {}
                        rows[layer]['layer'] = layer
                    for metric in sorted(metrics[folder][layer].keys()):
                        if not metrics_to_include or metric in metrics_to_include:
                            column_name = str(headers[folder]) + '_' + metric
                            if column_name not in columns:
                                columns.append(column_name)
                            rows[layer][column_name] = round(metrics[folder][layer][metric],2)
            f.write('% ' + dir + '\n')
            helper = '% ' + headers.__str__() + '\n'
            f.write(helper)
            table = []

            for layer in sorted(rows.keys()):
                row = [layer]
                for column in sorted(columns):
                    row.append(rows[layer][column])
                table.append(row)

            f.write(tabulate(table, tablefmt='latex', headers=['layer'] + sorted(columns)))
            f.write('\n % end of table \n')


def plot_summary(dir_metrics, dir_charts, metrics_to_include):
    """
    Save png images of the summary of the metrics saved in dir_metrics and save the images in dir_charts
    based on https://stackoverflow.com/a/14270539
    :param dir_metrics: path of directory where the metrics are saved
    :param dir_charts: path of directory where to save the images
    :param metrics_to_include: list of metrics to be included in the charts
    :return: png images of the different layers
    """

    #WE HAVE TO CHANGE THE NAME in the variable mask : (all stay first, and after in order of your file in the folder)
    folders = sorted([folder for folder in os.listdir(dir_metrics) if os.path.isdir(os.path.join(dir_metrics, folder))])
    mask = {'1all' : 'All', '2axel_alena': 'Ax & Al', '3axel_andrea': 'Ax & An', '4axel_aria': 'Ax & Ar', 
            '5axel_gosse': 'Ax & Go', 'alena_andrea': 'Al & An', 'alena_aria': 'Al & Ar', 'alena_gosse': 'Al & Go',
            'andrea_aria' : 'An & Ar', 'andrea_gosse' : 'An & Go', 'aria_gosse' : 'Ar & Go'}
    metrics_file = 'metrics.json'

    metrics = {}
    for folder in folders:
        with open(os.path.join(dir_metrics, folder, metrics_file)) as f:
            metrics[folder] = json.load(f)

    layers = sorted(metrics[folders[0]].keys())

    if not metrics_to_include:
        metrics_to_include = sorted(metrics[folders[0]][layers[0]].keys())

    #We have 17 COLORS : 
    colors = list(mcolors.BASE_COLORS.keys())
    colors.remove('w') # Delete white color
    colors2 = list(mcolors.TABLEAU_COLORS.keys())
    for color in colors2:
        colors.append(color)
        
    N_groups = len(metrics_to_include)
    location = np.arange(N_groups)
    width_bar = 0.062

    for layer in layers:
        #fig = plt.figure() #pour la taille standard
        fig = plt.figure(figsize = (12,7.5))
        ax = fig.add_subplot(111)

        groups = []

        for idx, folder in enumerate(folders):
            group = []
            for metric in metrics_to_include:
                group.append(metrics[folder][layer][metric])
            rect = ax.bar(location + (width_bar * idx), group, width_bar, color=colors[idx], label=mask[folder])
            groups.append(rect)

        ax.set_ylabel('Metrics')
        ax.set_xticks(location + width_bar)
        ax.set_xticklabels(tuple(metrics_to_include))
        #ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=len(folders), mode="expand", borderaxespad=0.)
        #modification position of the LEGEND
        ax.legend(bbox_to_anchor=(0.5, 0.5), loc=10, borderaxespad=0)

        def autolabel(rects):
            for rect in rects:
                h = rect.get_height()
                #POSITION of the number in every plot
                ax.text(rect.get_x() + rect.get_width() / 2., 1.03 * h, '%.2f' % float(h),
                        ha='center', va='bottom')
        for rect in groups:
            autolabel(rect)

        ax.set_title('Layer: ' + layer, loc='left')

        plt.savefig(dir_charts + '/' + layer + '_' + ".png")
        plt.close()


if __name__  == '__main__':
    # The metrics we want to see in the table, if we want all the metrics then this is not required
    metrics = ['Kappa', 'Avg_Ao']
    directory_paths = ['output_metrics/after_clean_calculation/metrics']
    #create_latex_table(directory_paths, 'output_metrics/latex/summary_before_after_cleaning.tex', metrics)
    plot_summary(directory_paths[0], 'output_metrics/summary/after_clean', metrics)
