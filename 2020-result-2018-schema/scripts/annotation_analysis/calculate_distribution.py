"""
Script for calculating frequency distributions for question/answer annotations
"""

import os
import pandas
import matplotlib.pyplot as plt


def main(language_files):
    """
    Calculate frequency distributions for question and answer annotations, and write the results to files
    :param language_files: annotation JSON files, organized by language (list of lists of filenames)
    :return:
    """
    # Question annotations
    for feature in ["question_type", "feature", "complexity", "is_quoted"]:
        calculate_distribution(language_files, "question", feature)

    # Answer annotations
    for feature in ["answer_type", "is_quoted"]:
        calculate_distribution(language_files, "answer", feature)


def calculate_distribution(language_files, annotation_type, feature):
    """
    Calculate the distribution for a specific annotatation type and layer, write the result to files
    :param language_files: annotation JSON files, organized by language (list of lists of filenames)
    :param annotation_type: type of annotation ("question" or "answer")
    :param feature: annotation layer (e.g. "question_type")
    :return:
    """

    combined = pandas.DataFrame()

    for language_name, files in language_files:

        data = [(f, get_annotation_values(f, annotation_type, feature)) for f in files]

        # Plot individual files
        for f, d in data:
            file_base = os.path.splitext(os.path.basename(f))[0]

            save_outputs(file_base, annotation_type, feature, d.value_counts(normalize=True))

        # Concatenate by language
        by_language = pandas.concat(d for f, d in data)
        by_language_freqs = by_language.value_counts(normalize=True)
        by_language_freqs.name = language_name
        combined = combined.append(by_language_freqs)
        save_outputs(language_name, annotation_type, feature, by_language_freqs)

    save_outputs("all", annotation_type, feature, combined)


def get_annotation_values(file, annotation_type, feature):
    """
    Extract annotation values from an annotation JSON file
    :param file: filename
    :param annotation_type: type of annotation ("question" or "answer")
    :param feature: annotation layer (e.g. "question_type")
    :return:
    """

    with open(file, encoding="utf-8") as f:
        annotations = pandas.read_json(f)
        annotations = annotations[annotations["annotation_type"] == annotation_type]
        feature_column = annotations[feature]

        # Clean up any underscores (for split annotations)
        feature_column = feature_column.replace(r"([A-Z][A-Z])\_\d", r"\1", regex=True)

        return feature_column


def save_outputs(file, annotation_type, feature, distribution):
    """
    Plot a distribution as a bar chart, and generate CSV and LaTeX tables
    :param file: filename
    :param annotation_type: type of annotation ("question" or "answer")
    :param feature: annotation layer (e.g. "question_type")
    :param distribution: pandas object containing the frequency distribution
    :return:
    """

    # Make and save the plot
    print("Saving chart for {} ({})".format(file, feature))

    plt.figure()
    plt.title(file + " " + feature)
    distribution.plot(kind="bar", stacked=True, rot=0).legend(
        bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    plt.ylabel("Relative frequency")
    for filetype in [".png", ".svg", ".pdf"]:
        plt.savefig("output_distribution/" + feature + "_" + annotation_type + "_" + file + filetype)
    plt.close()

    # Make and save as CSV
    print("Saving CSV for {} ({})".format(file, feature))
    distribution.to_csv("output_distribution/" + feature + "_" + annotation_type + "_" + file + ".csv", encoding="utf-8")

    # Make and save LaTeX table
    print("Saving LaTeX for {} ({})".format(file, feature))
    distribution.to_latex(buf="output_distribution/" + feature + "_" + annotation_type + "_" + file + ".tex",
                          bold_rows=True, na_rep="0")


if __name__ == '__main__':

    # Define which files should be analyzed
#    spanish = [
#        "../preprocessing/output/2034h.json",
#        "../preprocessing/output/2128h.json",
#        "../preprocessing/output/4053h.json"
#    ]
#
#    dutch = [
#        "../preprocessing/output/fn008000.json",
#        "../preprocessing/output/fn008001.json",
#        "../preprocessing/output/fn008003.json",
#    ]

    #CHANGE : the path of each .json file
    english = [
        "../preprocessing/output/amy_axel.json",
        "../preprocessing/output/amy_alena.json",
        "../preprocessing/output/amy_andrea.json",
        "../preprocessing/output/amy_aria.json",
        "../preprocessing/output/amy_gosse.json"
    ]

#    main([("Spanish", spanish), ("Dutch", dutch), ("English", english)])
    main([("English", english)])

