"""
Work for the project "Coffea or Tea?" (2019-2020) 
Script for extracting and aligning annotations from ELAN (*.eaf) files
Usage: python eaf_to_json.py [eaf-file] [output-name] [use-pretty-printing]
"""

import re
import bs4
import json
import sys

OVERLAP_MARGIN = 50


def main(eaf_file=None, output_name=None, use_pretty_printing=True):
    """
    Extract annotations from an ELAN file and write the result as JSON
    :param eaf_file: path to the ELAN file containing the annotations.
    :param output_name: name of the JSON output file (without the ".json" extension).
    """

    # The user file names SHOULD be supplied in the arguments AT THE END of this file.
    

    #IF YOU WORK ON ANOTHER EAF : please uncomment the 1st speakers and comment the second one
    #speakers = input("Enter the tier names of each speaker (space-separated): >>> ").split()
    speakers = ("*ADD *BRI").split()

    # Parse the EAF file, separate questions and answers
    annotations = parse_eaf(eaf_file, speakers)

    # Write the output file
    with open("output/" + output_name + ".json", "w", encoding="utf-8") as f:
        json.dump(annotations, f, indent=3 if use_pretty_printing else None)
        

def combine_split_annotations(annotations, target_field):

    result = []

    i = 0
    while i < len(annotations):

        # Search for indexed question/answer types (e.g. WH_1)
        if re.match(r"^[A-Z]+\_(?P<idx>\d+)$", annotations[i][target_field]):
            split_idx = 1
            to_merge = [annotations[i]]

            # Keep adding annotations to the list as long as the index keeps increasing
            while True:
                i += 1
                split_idx += 1

                if re.match(r"^[A-Z]+\_" + str(split_idx) + "$", annotations[i][target_field]):
                    to_merge.append(annotations[i])
                else:
                    break

            # Combine the annotations
            combined = to_merge[0]
            combined["text"] = " ".join(a["text"] for a in to_merge)
            combined["stop_time"] = to_merge[-1]["stop_time"]
            combined["merge_flag"] = True

            # Remove the underscore
            combined[target_field] = re.sub(r"^([A-Z]+)\_\d+", r"\1", combined[target_field])

            # Add to the list
            result.append(combined)

        else:
            result.append(annotations[i])
            i += 1

    return result


def parse_eaf(eaf_file, speakers):
    """
    Read an EAF file and extract question and answer annotations
    :param eaf_file: path to the ELAN input file
    :param speakers: list of tier names for each speaker
    :return: sorted list of all annotations
    """

    with open(eaf_file, encoding="utf-8") as f:
        soup = bs4.BeautifulSoup(f.read(), "lxml")
        time_slots = get_time_slots(soup)

        print("Finding utterances ...")
        utterances = []

        for tier in speakers:
            utterances.extend(get_annotations(soup, time_slots, tier))

        utterances.sort(key=lambda entry: entry["start_time"])

        print("Finding question annotations ...")
        
        #QUESTION OF ADD ==============================
        question_id1 = list(get_annotations(soup, time_slots, tier_id="1_Question"))
        question_form1 = list(get_annotations(soup, time_slots, tier_id="1_Q_Form"))
        question_dis1 = list(get_annotations(soup, time_slots, tier_id="1_Q_DISJ"))
        features1 = list(get_annotations(soup, time_slots, tier_id="1_Q_Feature"))
        quoted1 = list(get_annotations(soup, time_slots, tier_id="1_Q_Quoted"))
        phatic1 = list(get_annotations(soup, time_slots, tier_id="1_Q_Phatic"))
        multiple1 = list(get_annotations(soup, time_slots, tier_id="1_Q_Multiple"))
        implied_form1 = list(get_annotations(soup, time_slots, tier_id="1_Q_Implied_Form"))
        clarification1 = list(get_annotations(soup, time_slots, tier_id="1_Q_Clarification"))
        
        print("Aligning question annotations for ADD ...")
        questions1 = list(
            find_questions(utterances, question_id1, question_form1, question_dis1,
                           features1, quoted1, phatic1, multiple1, implied_form1, clarification1)
        )
        questions1 = combine_split_annotations(questions1, "question_form")
        
        
        #QUESTION OF BRI ==============================
        question_id2 = list(get_annotations(soup, time_slots, tier_id="2_Question"))
        question_form2 = list(get_annotations(soup, time_slots, tier_id="2_Q_Form"))
        question_dis2 = list(get_annotations(soup, time_slots, tier_id="2_Q_DISJ"))
        features2 = list(get_annotations(soup, time_slots, tier_id="2_Q_Feature"))
        quoted2 = list(get_annotations(soup, time_slots, tier_id="2_Q_Quoted"))
        phatic2 = list(get_annotations(soup, time_slots, tier_id="2_Q_Phatic"))
        multiple2 = list(get_annotations(soup, time_slots, tier_id="2_Q_Multiple"))
        implied_form2 = list(get_annotations(soup, time_slots, tier_id="2_Q_Implied_Form"))
        clarification2 = list(get_annotations(soup, time_slots, tier_id="2_Q_Clarification"))

        print("Aligning question annotations for BRI ...")
        questions2 = list(
            find_questions(utterances, question_id2, question_form2, question_dis2,
                           features2, quoted2, phatic2, multiple2, implied_form2, clarification2)
        )
        questions2 = combine_split_annotations(questions2, "question_form")
        
        
        #ANSWERS OF ADD ==============================
        print("Finding and aligning answers of ADD ...")
        
        answer_id1 = list(get_annotations(soup, time_slots, tier_id="1_Answer"))
        answer_types1 = list(get_annotations(soup, time_slots, tier_id="1_A_Type"))
        answer_form1 = list(get_annotations(soup, time_slots, tier_id="1_A_Form"))
        answer_feat1 = list(get_annotations(soup, time_slots, tier_id="1_A_Feature"))
        aquoted1 = list(get_annotations(soup, time_slots, tier_id="1_A_Quoted"))
        aphatic1 = list(get_annotations(soup, time_slots, tier_id="1_A_Phatic"))
        amultiple1 = list(get_annotations(soup, time_slots, tier_id="1_A_Multiple"))
        aimplied_form1 = list(get_annotations(soup, time_slots, tier_id="1_A_Implied_Form"))
        amismatch1 = list(get_annotations(soup, time_slots, tier_id="1_A_Mismatch"))
        
        answers1 = list(
            find_answers(utterances, answer_id1, answer_types1, answer_form1, answer_feat1,
                         aquoted1, aphatic1, amultiple1, aimplied_form1, amismatch1)
        )
        answers1 = combine_split_annotations(answers1, "answer_type")
        
        
        #ANSWERS OF BRI ==============================
        print("Finding and aligning answers of BRI ...")
        
        answer_id2 = list(get_annotations(soup, time_slots, tier_id="2_Answer"))
        answer_types2 = list(get_annotations(soup, time_slots, tier_id="2_A_Type"))
        answer_form2 = list(get_annotations(soup, time_slots, tier_id="2_A_Form"))
        answer_feat2 = list(get_annotations(soup, time_slots, tier_id="2_A_Feature"))
        aquoted2 = list(get_annotations(soup, time_slots, tier_id="2_A_Quoted"))
        aphatic2 = list(get_annotations(soup, time_slots, tier_id="2_A_Phatic"))
        amultiple2 = list(get_annotations(soup, time_slots, tier_id="2_A_Multiple"))
        aimplied_form2 = list(get_annotations(soup, time_slots, tier_id="2_A_Implied_Form"))
        amismatch2 = list(get_annotations(soup, time_slots, tier_id="2_A_Mismatch"))

        answers2 = list(
            find_answers(utterances, answer_id2, answer_types2, answer_form2, answer_feat2,
                         aquoted2, aphatic2, amultiple2, aimplied_form2, amismatch2)
        )
        answers2 = combine_split_annotations(answers2, "answer_type")


        #return a sorted list of all the Q-A
        return sorted(questions1 + answers1 + questions2 + answers2, key=lambda e: e["start_time"])


def get_time_slots(soup):
    """
    Extracts time slots from the EAF file
    :param soup: BeautifulSoup object corresponding to the EAF file
    :return: dictionary mapping time slot ids (as string) to time values (as string)
    """

    time_slots = {}

    for time_slot in soup.find_all("time_slot"):

        time_slots[time_slot["time_slot_id"]] = time_slot["time_value"]

    return time_slots


def get_annotations(soup, time_slots, tier_id="", index=0):
    """
    Extract annotations from a specified layer in the ELAN file
    :param soup: BeautifulSoup object corresponding to the EAF file
    :param time_slots: time slot dictionary (see `get_time_slots()`)
    :param tier_id: name of the tier to extract annotations from. If not supplied, index will be used instead.
    :param index: index of the tier to extract annotations from. Defaults to the first tier; this parameter is not used
    if a `tier_id` is supplied.
    :return: list of dictionaries representing each annotation (fields: "tier", "start_time", "stop_time", "text")
    """

    # Select the right tier
    if tier_id:
        tier = soup.find("tier", attrs={"tier_id": tier_id})
    else:
        tier = soup.find_all("tier")[index]
    
    # Construct dictionary entries
    for annotation in tier.find_all("alignable_annotation"):
        utterance = {
            "tier": tier_id,
            "start_time": float(time_slots[annotation["time_slot_ref1"]]),
            "stop_time": float(time_slots[annotation["time_slot_ref2"]]),
            "text": annotation.find("annotation_value").text
        }

        # Skip utterances with no text
        if utterance["text"] != "":
            yield utterance


def find_prev_utterance(utterance_matches, utterances):
    """
    :param utterance_matches: list of utterances that matched for a given question
    :param utterances: list of all utterance annotations (= transcribed text)
    :return: utterances
    """
    if not (utterances and utterance_matches):
        return None

    for utterance in utterances:

        if utterance["stop_time"] >= utterance_matches[0]["start_time"]:
            return utterance

    return None


def find_questions(utterances, question_types, question_form, question_dis, features, quoted, phatic, multiple, implied_form, clarification):
    """
    Collect and align annotations for each question_type
    :param utterances: list of utterance annotations (= transcribed text)
    :param : list of id, form, disjonction, feature, quoted, phatic, multiple, implied, clarification
    :return: generator that yields a dictionary for each aligned annotation
    """

    for question in question_types:
        
        utterance = find_matching_entry(question, utterances)
        utterance_text = utterance["text"] if utterance else None
        prev_utterance = find_prev_utterance([utterance] if utterance else [], utterances)

        # list of parameters
        form_match = find_matching_entry(question, question_form)
        disj_match = find_matching_entry(question, question_dis)
        feature_match = find_matching_entry(question, features)
        quoted_match = find_matching_entry(question, quoted)
        phatic_match = find_matching_entry(question, phatic)
        multiple_match = find_matching_entry(question, multiple)
        implied_match = find_matching_entry(question, implied_form)
        clarification_match = find_matching_entry(question, clarification)

#        print(question["text"])
        
        yield {
            "annotation_type": "question",
            "id_question" : question["text"],
            "text": utterance_text,
            "speaker": utterance["tier"] if utterance else None,
            "start_time": question["start_time"],
            "stop_time": question["stop_time"],
            "question_form": form_match["text"] if form_match else "None",
            "question_disjonction" : disj_match["text"] if disj_match else None,
            "question_feature": feature_match["text"] if feature_match else None,
            "question_quoted": quoted_match["text"] if quoted_match else None,
            "question_phatic": phatic_match["text"] if phatic_match else None,
            "question_multiple" : multiple_match["text"] if multiple_match else None,
            "question_impliedform" : implied_match["text"] if implied_match else None,
            "question_clarification" : clarification_match["text"] if clarification_match else None,
            "prev_speaker": prev_utterance["tier"] if prev_utterance else None,
            "prev_text": prev_utterance["text"] if prev_utterance else None
        }
        

def find_answers(utterances, answer_id, answer_types, answer_form, answer_feat, aquoted, aphatic, amultiple, aimplied_form, amismatch):
    """
    Collect and align annotations for each answer
    :param answer_types: list of answer type annotations
    :param : list of id, types, form, feature, quoted, phatic, multiple, impliedform, mismatch
    :return: generator that yields a dictionary for each aligned annotations
    """

    for answer in answer_id:

        utterance = find_matching_entry(answer, utterances)
        utterance_text = utterance["text"]
        
        # list of parameters
        answer_type = find_matching_entry(answer, answer_types)
        aform_match = find_matching_entry(answer, answer_form)
        afeature_match = find_matching_entry(answer, answer_feat)
        aquoted_match = find_matching_entry(answer, aquoted)
        aphatic_match = find_matching_entry(answer, aphatic)
        amultiple_match = find_matching_entry(answer, amultiple)
        aimplied_match = find_matching_entry(answer, aimplied_form)
        amismatch_match = find_matching_entry(answer, amismatch)
        
#        print(answer["text"])

        yield {
            "annotation_type": "answer",
            "id_answer" : answer["text"],
            "text": utterance_text,
            "speaker": utterance["tier"] if utterance else None,
            "start_time": answer["start_time"],
            "stop_time": answer["stop_time"],
            "answer_type": answer_type["text"] if answer_type else "None",
            "answer_form" : aform_match["text"] if aform_match else None,
            "answer_feature" : afeature_match["text"] if afeature_match else None,
            "answer_quoted" : aquoted_match["text"] if aquoted_match else None,
            "answer_phatic" : aphatic_match["text"] if aphatic_match else None,
            "answer_multiple" : amultiple_match["text"] if amultiple_match else None,
            "answer_impliedform" : aimplied_match["text"] if aimplied_match else None,
            "answer_mismatch" : amismatch_match["text"] if amismatch_match else None
        }


def find_matching_entry(entry, entries):
    """
    :param entry:
    :param entries:
    :return:
    """
    # Try to find an entry that starts at least as early and stops at least as late
    for e in entries:

        if (e["start_time"] - OVERLAP_MARGIN <= entry["start_time"] and
            e["stop_time"] + OVERLAP_MARGIN >= entry["stop_time"]) or \
                (e["start_time"] >= entry["start_time"] and
                 e["stop_time"] <= entry["stop_time"]):
            return e

    return None




if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python eaf_to_json.py [eaf-file] [output-name] [use-pretty-printing]")
        exit(1)

    #You have to change the PATH to your .eaf file, and the name of the .json file
    main("C:/Users/Kroxyl/Desktop/S8 Supervised Project/809-question-annotation-2018-master - Copie/corpus/EAF/amy_2020_alena.eaf", "amy_alena_2020", True)


