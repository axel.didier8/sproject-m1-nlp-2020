"""
Script for extracting and aligning annotations from ELAN (*.eaf) files

Usage: python eaf_to_json.py [eaf-file] [output-name] [use-pretty-printing]
(If an empty string is passed for eaf-file or output-name, the script will prompt the user to
enter these filenames)
"""

import re

import bs4
import json
import sys

OVERLAP_MARGIN = 50


def main(eaf_file=None, output_name=None, use_pretty_printing=True):
    """
    Extract annotations from an ELAN file and write the result as JSON
    :param eaf_file: path to the ELAN file containing the annotations. By default, ask the user.
    :param output_name: name of the JSON output file (without the ".json" extension). By default, ask the user.
    :param use_pretty_printing: if True (by default), pretty-print the JSON-file, if not, use a compact version.
    :return:
    """

    # Ask the user for file names if they are not supplied in the arguments
    if not eaf_file:
        eaf_file = input("EAF file: ")
    if not output_name:
        output_name = input("Output name: ")

    print("if you work on amy.eaf you should put '*ADD *BRI' to the next enter.")
    speakers = input("Enter the tier names of each speaker (space-separated): >>> ").split()

    # Are annotations word-by-word (Dutch corpus only)?
    word_by_word = input("Word-by-word (Dutch?) [yes/no] >>> ").lower() == "yes"

    # Parse the EAF file, separate questions and answers
    annotations = parse_eaf(eaf_file, word_by_word, speakers)
    print("annotations")
    print(annotations)
    questions = [a for a in annotations if a["annotation_type"] == "question"]
    print("question")
    print(questions)
    answers = [a for a in annotations if a["annotation_type"] == "answer"]

    # Write the output file
    with open("output/" + output_name + ".json", "w", encoding="utf-8") as f:
        json.dump(annotations, f, indent=3 if use_pretty_printing else None)

    # Find potential problems and print warning messages
    q_missing_text = [q for q in questions if not q["text"]]
    q_missing_complexity = [q for q in questions if not q["complexity"]]
    q_missing_quoted = [q for q in questions if not q["is_quoted"]]
    a_missing_text = [a for a in answers if not a["text"]]
    a_missing_quoted = [a for a in answers if not a["is_quoted"]]
    overlap_problems = [a for a in annotations if a["overlap_flag"]]
    #partial_problems = [a for a in annotations if a["partial_flag"]]
    print("\nFound {} questions and {} answers, wrote to JSON".format(len(questions), len(answers)))
    print("\nWARNINGS:")
    print("{} question(s) and {} answer(s) are missing text".format(len(q_missing_text), len(a_missing_text)))
    print("{} question(s) and {} answer(s) are missing an is_quoted annotation".format(len(q_missing_quoted),
                                                                                       len(a_missing_quoted)))
    print("{} question(s) are missing a complexity annotation".format(len(q_missing_complexity)))
    print("{} annotation(s) have potential overlap problems".format(len(overlap_problems)))
    print("{} annotation(s) seem to be partial (smaller than the utterance)".format(format(len(partial_problems))))


def combine_split_annotations(annotations, target_field):

    result = []

    i = 0
    while i < len(annotations):

        # Search for indexed question/answer types (e.g. WH_1)
        if re.match(r"^[A-Z]+\_(?P<idx>\d+)$", annotations[i][target_field]):
            split_idx = 1
            to_merge = [annotations[i]]

            # Keep adding annotations to the list as long as the index keeps increasing
            while True:
                i += 1
                split_idx += 1

                if re.match(r"^[A-Z]+\_" + str(split_idx) + "$", annotations[i][target_field]):
                    to_merge.append(annotations[i])
                else:
                    break

            # Combine the annotations
            combined = to_merge[0]
            combined["text"] = " ".join(a["text"] for a in to_merge)
            combined["stop_time"] = to_merge[-1]["stop_time"]
            combined["merge_flag"] = True

            # Remove the underscore
            combined[target_field] = re.sub(r"^([A-Z]+)\_\d+", r"\1", combined[target_field])

            # Add to the list
            result.append(combined)

        else:
            result.append(annotations[i])
            i += 1

    return result


def parse_eaf(eaf_file, word_by_word, speakers):
    """
    Read an EAF file and extract question and answer annotations
    :param eaf_file: path to the ELAN input file
    :param word_by_word: are the annotations word-by-word or utterance-by-utterance?
    :param speakers: list of tier names for each speaker
    :return: sorted list of all annotations
    """

    with open(eaf_file, encoding="utf-8") as f:
        soup = bs4.BeautifulSoup(f.read(), "lxml")
        time_slots = get_time_slots(soup)

        print("Finding utterances ...")
        utterances = []

        for tier in speakers:
            utterances.extend(get_annotations(soup, time_slots, tier))

        utterances.sort(key=lambda entry: entry["start_time"])

        print("Finding question annotations ...")
        question_types = list(get_annotations(soup, time_slots, tier_id="Question_Type"))
        print("je suis ici")
        print(question_types)
        features = list(get_annotations(soup, time_slots, tier_id="Feature"))
        complexity_list = list(get_annotations(soup, time_slots, tier_id="Complexity"))
        is_quoted_list = list(get_annotations(soup, time_slots, tier_id="Is_Quoted"))

        print("Aligning question annotations ...")
        questions = list(
            find_questions(utterances, question_types, features, complexity_list, is_quoted_list, word_by_word)
        )

        questions = combine_split_annotations(questions, "question_type")

        print("Finding and aligning answers ...")
        answer_types = list(get_annotations(soup, time_slots, tier_id="Answer_Type"))
        answers = list(find_answers(answer_types, utterances, is_quoted_list, word_by_word))
        answers = combine_split_annotations(answers, "answer_type")

        return sorted(questions + answers, key=lambda e: e["start_time"])


def get_time_slots(soup):
    """
    Extracts time slots from the EAF file
    :param soup: BeautifulSoup object corresponding to the EAF file
    :return: dictionary mapping time slot ids (as string) to time values (as string)
    """

    time_slots = {}

    for time_slot in soup.find_all("time_slot"):

        time_slots[time_slot["time_slot_id"]] = time_slot["time_value"]

    return time_slots


def get_annotations(soup, time_slots, tier_id="", index=0):
    """
    Extract annotations from a specified layer in the ELAN file
    :param soup: BeautifulSoup object corresponding to the EAF file
    :param time_slots: time slot dictionary (see `get_time_slots()`)
    :param tier_id: name of the tier to extract annotations from. If not supplied, index will be used instead.
    :param index: index of the tier to extract annotations from. Defaults to the first tier; this parameter is not used
    if a `tier_id` is supplied.
    :return: list of dictionaries representing each annotation (fields: "tier", "start_time", "stop_time", "text")
    """

    # Select the right tier
    if tier_id:
        tier = soup.find("tier", attrs={"tier_id": tier_id})
    else:
        tier = soup.find_all("tier")[index]

    # Construct dictionary entries
    for annotation in tier.find_all("alignable_annotation"):
        utterance = {
            "tier": tier_id,
            "start_time": float(time_slots[annotation["time_slot_ref1"]]),
            "stop_time": float(time_slots[annotation["time_slot_ref2"]]),
            "text": annotation.find("annotation_value").text
        }

        # Skip utterances with no text
        if utterance["text"] != "":
            yield utterance


def find_prev_utterance(utterance_matches, utterances):
    """

    :param utterance_matches: list of utterances that matched for a given question
    :param utterances: list of all utterance annotations (= transcribed text)
    :return:
    """
    if not (utterances and utterance_matches):
        return None

    for utterance in utterances:

        if utterance["stop_time"] >= utterance_matches[0]["start_time"]:
            return utterance

    return None


def find_questions(utterances, question_types, features, complexity_list, is_quoted_list, word_by_word):
    """
    Collect and align annotations for each question
    :param utterances: list of utterance annotations (= transcribed text)
    :param question_types: list of question type annotations
    :param features: list of feature annotations
    :param complexity_list: list of complexity annotations
    :param is_quoted_list: list of is_quoted annotations
    :param word_by_word: are the annotations word-by-word or utterance-by-utterance?
    :return: generator that yields a dictionary for each aligned annotation
    """

    for question in question_types:
        if not word_by_word:
            utterance = find_matching_entry(question, utterances)
            utterance_text = utterance["text"] if utterance else None
            prev_utterance = find_prev_utterance([utterance] if utterance else [], utterances)
            overlap_flag = False
            partial_flag = utterance and (question["start_time"] > utterance["start_time"] or
                                          question["stop_time"] < utterance["stop_time"])

        else:
            utterance_matches, overlap_flag, partial_flag = find_overlapping_entries(question, utterances)
            prev_utterance = find_prev_utterance(utterance_matches, utterances)
            utterance = utterance_matches[0] if utterance_matches else None
            utterance_text = " ".join(u["text"] for u in utterance_matches) if utterance_matches else None

        feature_match = find_matching_entry(question, features)
        is_quoted_match = find_matching_entry(question, is_quoted_list)
        complexity_match = find_matching_entry(question, complexity_list)

        yield {
            "annotation_type": "question",
            "text": utterance_text,
            "speaker": utterance["tier"] if utterance else None,
            "start_time": question["start_time"],
            "stop_time": question["stop_time"],
            "question_type": question["text"],
            "feature": feature_match["text"] if feature_match else None,
            "is_quoted": is_quoted_match["text"] if is_quoted_match else None,
            "complexity": complexity_match["text"] if complexity_match else None,
            "overlap_flag": overlap_flag,
            "partial_flag": partial_flag,
            "merge_flag": False,
            "prev_text": prev_utterance["text"] if prev_utterance else None,
            "prev_speaker": prev_utterance["tier"] if prev_utterance else None
        }


def find_answers(answer_types, utterances, is_quoted_list, word_by_word):
    """
    Collect and align annotations for each answer
    :param answer_types: list of answer type annotations
    :param utterances: list of utterance annotations (= transcribed text)
    :param is_quoted_list: list of is_quoted annotations
    :param word_by_word: are the annotations word-by-word or utterance-by-utterance?
    :return: generator that yields a dictionary for each aligned annotations
    """

    for answer in answer_types:

        if not word_by_word:
            utterance = find_matching_entry(answer, utterances)
            utterance_text = utterance["text"]
            overlap_flag = False
            partial_flag = utterance and (answer["start_time"] > utterance["start_time"] or
                                          answer["stop_time"] < utterance["stop_time"])

        else:
            utterance_matches, overlap_flag, partial_flag = find_overlapping_entries(answer, utterances)
            utterance = utterance_matches[0] if utterance_matches else None
            utterance_text = " ".join(u["text"] for u in utterance_matches) if utterance_matches else None

        is_quoted_match = find_matching_entry(answer, is_quoted_list)

        yield {
            "annotation_type": "answer",
            "text": utterance_text,
            "speaker": utterance["tier"] if utterance else None,
            "start_time": answer["start_time"],
            "stop_time": answer["stop_time"],
            "answer_type": answer["text"],
            "is_quoted": is_quoted_match["text"] if is_quoted_match else None,
            "overlap_flag": overlap_flag,
            "partial_flag": partial_flag,
            "merge_flag": False
        }


def find_overlapping_entries(entry, entries):
    """
    Find temporally overlapping annotations, in order to align them
    :param entry: a single annotation (in tier X)
    :param entries: list of annotations (in tier Y)
    :return:
    """

    # Skip over entries that are before the current one
    i = 0
    while i < len(entries) and entries[i]["stop_time"] < entry["start_time"]:
        i += 1

    # Add all the overlapping entries
    overlaps = []
    while i < len(entries) and entries[i]["start_time"] < entry["stop_time"]:
        # Filter out very small overlaps
        if abs(entries[i]["stop_time"] - entry["start_time"]) < OVERLAP_MARGIN or \
                abs(entries[i]["start_time"] - entry["stop_time"]) < OVERLAP_MARGIN:
            i += 1
        else:
            overlaps.append(entries[i])
            i += 1

    # Check if the overlaps have the same tier
    overlap_flag = False

    if overlaps:
        tier = overlaps[0]["tier"]
        for i in overlaps[1:]:
            if i["tier"] != tier:
                print("Tier mismatch: {} vs {}".format(i["tier"], tier))
                overlap_flag = True
                break

    # Check if `entry`'s time span is shorter than the combined overlaps' time spans
    partial_flag = overlaps and entry["stop_time"] - entry["start_time"] < \
                   overlaps[-1]["stop_time"] - overlaps[0]["start_time"]

    return overlaps, overlap_flag, partial_flag


def find_matching_entry(entry, entries):
    """

    :param entry:
    :param entries:
    :return:
    """
    # Try to find an entry that starts at least as early and stops at least as late
    for e in entries:

        if (e["start_time"] - OVERLAP_MARGIN <= entry["start_time"] and
            e["stop_time"] + OVERLAP_MARGIN >= entry["stop_time"]) or \
                (e["start_time"] >= entry["start_time"] and
                 e["stop_time"] <= entry["stop_time"]):
            return e

    return None


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python eaf_to_json.py [eaf-file] [output-name] [use-pretty-printing]")
        exit(1)

    #You have to change the PATH to your .eaf file, and how you want to remane the .json file that the script create
    main("C:/Users/Kroxyl/Desktop/S8 Supervised Project/809-question-annotation-2018-master - Copie/corpus/EAF/amy_alena.eaf", "amy_alena", True)
