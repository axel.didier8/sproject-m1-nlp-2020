"""
Script for calculating agreement statistics
"""

import csv
import itertools
import json
from collections import defaultdict

from nltk.metrics.agreement import AnnotationTask
from sklearn.metrics import cohen_kappa_score


def get_annotations_dict(file_paths):
    """
    Obtains the annotations from json files and return the dictionaries
    :param file_paths: List of string containing the path of the json files to be analyzed
    :return: List of dictionaries of the annotations
    """
    # Load all json files
    annotations = []

    for file in file_paths:
        with open(file, 'r') as f:
            annotations.append(json.load(f))
    return annotations


def belongs_to_interval(annotation, timespans):
    """
    Decides whether an annotation belongs to an interval or not
    :param annotation: Dictionary with the values for the annotation
    :param timespans: List of time spans
    :return: Boolean indicating if the annotation belongs or not to some timespan
    """
    for timespan in timespans:
        if annotation['start_time'] >= timespan[0] and annotation['stop_time'] <= timespan[1]:
            return True

    return False


def get_annotations_from_timespans(annotations, timespans):
    """
    Filter the list of annotations according to timespans. Only the annotations in the intervals will be keep them
    in the final list
    :param annotations: list of annotations per annotator
    :return: Filter list of the annotations
    """
    filtered_annotations = [[] for annotator in annotations]
    
    for idx in range(0, len(annotations)):
        while len(annotations[idx]):
            if belongs_to_interval(annotations[idx][0], timespans):
                filtered_annotations[idx].append(annotations[idx].pop(0))
            else:
                annotations[idx].pop(0)

    return filtered_annotations


def are_there_elements(annotations):
    """
    Decides whether or not there are elements in the lists
    :param annotations: A list of lists of dictionaries
    :return: Boolean indicating if there are elements
    """

    if True in [len(annotation)>0 for annotation in annotations]:
        return True
    else:
        return False


def is_same_item(time, text, item):
    """
    Decides whether the item is the same as the one which has time and text.
    :param time: Time of the first item
    :param text: Text of utterance of the first item
    :param item: Item with want to compare with the first item
    :return: Boolean indicating if the item is the same or not
    """

    if item:
        if item['start_time'] - time <= 50 and item['text'] == text: # Same value used to generate the json files
            return True
        else:
            return False
    else:
        return False

def calculate_annotation_task(file_paths, timespans):
    """
    Calculate the annotation task used for calculate the metrics, and disagreement
    :param file_paths: List of strings containing the path of the json files to be compared.
    :param timespans: List of tuples (init, end) defining the timespans where all annotators worked.
    :return: A dictionary of lists of triples (annotations per layer) and A dictionary with the utterances annotated
    per layer.
    """
    
    # Layers according to question and answer
    question_layers = ['question_form', 'question_feature', 'question_disjonction', 'question_quoted', 'question_phatic', 'question_multiple', 'question_impliedform', 'question_clarification']
    answer_layers = ['answer_type', 'answer_form', 'answer_feature', 'answer_quoted', 'answer_phatic', 'answer_multiple', 'answer_impliedform', 'answer_mismatch']

    # Load all json files and number of annotators
    annotations = get_annotations_dict(file_paths)
    annotators = len(annotations)

    # Extract the annotations to be analyzed if there is timespans
    if timespans:
        annotations = get_annotations_from_timespans(annotations, timespans)

    # Create the annotation task
    annotations_task = defaultdict(list)
    item_text = defaultdict(list)
    item_time = defaultdict(list)
    annotations_list = annotations.copy()
    index = defaultdict(int)

    while are_there_elements(annotations_list):
        items = []

        for idx, annotation in enumerate(annotations_list):
            if len(annotation) > 0: # The annotation list still has elements
                items.append((idx, annotation[0]))

        items = sorted(items, key=lambda x:  x[1]['start_time'])

        first_time = items[0][1]['start_time']
        first_text = items[0][1]['text']

        # annotations of the same timespan (close to each other at least 50 points) and same text for the utterance.
        # These are the annotations to be included
        same_time_items = [item for item in items if is_same_item(first_time, first_text, item[1])]
        # There is the case in which the annotators disagree with respect of the type of the utterance
        # that is why it is necessary to split the annotations to be include into questions and answers
        questions = [item for item in same_time_items if 'question_form' in item[1]]
        answers = [item for item in same_time_items if 'answer_type' in item[1]]

        # Include empty annotations for the annotators who do not annotate the utterance to be added
        if questions:
            missing_annotations_q = [idx for idx in range(0, annotators) if idx not in [item[0] for item in questions]]
        else:
            missing_annotations_q = []

        if answers:
            missing_annotations_a = [idx for idx in range(0, annotators) if idx not in [item[0] for item in answers]]
        else:
            missing_annotations_a = []

        for item in missing_annotations_q:
            questions.append((item, None))

        for item in missing_annotations_a:
            answers.append((item, None))

        for layer in question_layers:
            for item in questions:
                label = ''
                if item[1]:
                    label = item[1][layer]

                if label:
                    label = label.split('_')[0]
                else:
                    label = ''
                annotations_task[layer].append(
                    # Triple (coder, item, label)
                    (file_paths[item[0]], index[layer], label)
                )
            if questions: # There were questions
                item_text[layer].append((index[layer], questions[0][1]['text']))
                item_time[layer].append((index[layer], questions[0][1]['start_time']))
                index[layer] += 1

        # Answers
        for layer in answer_layers:
            for item in answers:
                label = ''
                if item[1]:
                    label = item[1][layer]
                if label:
                    label = label.split('_')[0]
                else:
                    label = ''
                annotations_task[layer].append(
                    # Triple (coder, item, label)
                    (file_paths[item[0]], index[layer], label)
                )
            if answers: # There were answers
                item_text[layer].append((index[layer], answers[0][1]['text']))
                item_time[layer].append((index[layer], answers[0][1]['start_time']))
                index[layer] += 1

        # Delete items added
        for item in same_time_items:
            annotations_list[item[0]].pop(0)

    return annotations_task, item_text, item_time


def write_all_csv(annotations_task, item_text, item_time, file_paths, path):
    """
    Write the csv for all the layers in the annotations task in the path specified.
    :param annotaions_task: A dictionary of lists of triples (annotations per layer)
    :param item_text: A dictionary of the utterances annotated
    :param path: String of the path where the files should be saved
    :return: csv files in the specified path
    """
    # Create files with annotations for different layers
    for layer in annotations_task.keys():
        write_csv_file(layer, annotations_task[layer], file_paths, item_text[layer], item_time[layer], path)


def calculate_metrics(annotations_task, path):
    """
    Calculates the inter-annotator agreement according to the annotations using nltk library
    :param annotations_task: A dictionary of lists of triples (annotations per layer)
    :param path: A string with the path where the json file should be saved
    :return: A dictionary with the metrics, and a json file in the path specified
    """
    # metrics
    metrics = {}
    for layer in annotations_task.keys():
        task = AnnotationTask(annotations_task[layer])
        metrics[layer] = {}

        # Kappa
        try:
            metrics[layer]['Kappa'] = task.kappa()
        except:
            metrics[layer]['Kappa'] = 1

        # Other metrics
        metrics[layer]['Avg_Ao'] = task.avg_Ao()
        try:
            metrics[layer]['S'] = task.S()
        except:
            metrics[layer]['S'] = 1
        try:
            metrics[layer]['PI'] = task.pi()
        except:
            metrics[layer]['PI'] = 1

    with open(path, 'w', encoding='utf-8') as f:
        json.dump(metrics, f)

    return metrics


def calculate_kappa_sklearn(annotations_task, classes, path):
    """
    Calculate the kappa score for each layer in annotations_taks using sklearn library
    :param annotations_task: A dictionary of lists of the annotations per layer (triples)
    :param classes: A dictionary with the list of the possible classes per layer
    :param path: A string with the path where the json file should be saved
    :return: A dictionoary with the kappa score for each layer and a json file in the path specified
    """
    kappa_scores = {}

    # add missing annotation class = '' (empty string)
    if classes:
        for layer in classes.keys():
            classes[layer].append('')

    for layer in annotations_task.keys():
        annotations_arrays = []  # Data structure to represent the annotations for sklearn
        annotations = {}  # helper data structure

        annotation_list = annotations_task[layer]
        for annotation in annotation_list:
            if annotation[0] not in annotations:
                annotations[annotation[0]] = {}
            annotations[annotation[0]][annotation[1]] = annotation[2]

        for annotator in annotations:
            annotations_arrays.append([item[1] for item in sorted(annotations[annotator].items())])

        # Calculate metric
        kappa_pairwise = []
        for pair in itertools.combinations([idx for idx in range(0, len(annotations_arrays))], 2):
            if classes:
                tmp_kappa = cohen_kappa_score(annotations_arrays[pair[0]], annotations_arrays[pair[1]],
                                              labels=classes[layer])
            else:
                tmp_kappa = cohen_kappa_score(annotations_arrays[pair[0]], annotations_arrays[pair[1]])
            kappa_pairwise.append(tmp_kappa)

        kappa_scores[layer] = sum(kappa_pairwise) / len(kappa_pairwise)

    with open(path, 'w', encoding='utf-8') as f:
        json.dump(kappa_scores, f)

    return kappa_scores



def write_csv_file(layer, annotations, annotators, text_list, time_list, path):
    """
    Writes a csv file with the annotations of each annotator for the layer
    :param layer: String with the name of the layer
    :param annotations: The annotations of all annotators for the layer
    :param annotators: List of the names of the annotators
    :param text_list: List of the utterances annotated
    :param path: String with the path where the file should be saved
    :return: a csv file with the metrics in the path specify
    """

    last_item = annotations[-1][1]
    # Name of columns
    items = ['item-' + str(idx) for idx in range(0, last_item + 1)]
    fieldnames = ['annotator'] + items



    rows = []
    rows_disagreement = []
    annotators_dict = {annotator: {} for annotator in annotators}

    # Create dictionaries per annotator with the annotations for each item
    for annotation in annotations:
        annotators_dict[annotation[0]]['item-' + str(annotation[1])] = annotation[2]

    annotators_dict['text'] = {}
    annotators_dict['time'] = {}
    for idx, text in enumerate(text_list):
        annotators_dict['text']['item-' + str(text[0])] = text[1]
        annotators_dict['time']['item-' + str(text[0])] = time_list[idx][1]

    # Create rows for file
    for annotator in annotators_dict.keys():
        row = {'annotator': annotator}
        for item in annotators_dict[annotator]:
            row[item] = annotators_dict[annotator][item]
        rows.append(row)

    # Create rows for disagreement
    annotators_dict_disagreement = {annotator:{} for annotator in annotators}
    for item in items:
        annotation_tags = [(annotator, annotators_dict[annotator][item])
                           for annotator in annotators if item in annotators_dict[annotator]]
        disagreement = False
        if len(annotation_tags) != len(annotators):
            # There is a disagreement, not all annotators tag the utterance
            disagreement = True
        elif len({annotation_tag[1] for annotation_tag in annotation_tags}) != 1:
            # There is a disagreement although all annotators gave a tag
            disagreement = True

        if disagreement:
            for annotation in annotation_tags:
                annotators_dict_disagreement[annotation[0]][item] = annotation[1]

    items_set = set()
    for annotator in annotators:
        row = {'annotator': annotator}
        for item in annotators_dict_disagreement[annotator]:
            row[item] = annotators_dict_disagreement[annotator][item]
            items_set.add(item)
        rows_disagreement.append(row)

    row_text_disagreement = {'annotator': 'text'}
    row_time_disagreement = {'annotator': 'time'}
    for idx, text in enumerate(text_list):
        if 'item-' + str(text[0]) in items_set:
            row_text_disagreement['item-' + str(text[0])] = text[1]
            row_time_disagreement['item-' + str(text[0])] = time_list[idx][1]
    rows_disagreement.append(row_text_disagreement)
    rows_disagreement.append(row_time_disagreement)

    fieldnames_disagreement = ['annotator'] + sorted(list(items_set))

    # Write the csv with all the annotations
    with open(path + layer + '.csv', 'w', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(rows)

    # Write the csv with only the disagreement
    with open(path + layer + '_disagreement.csv', 'w', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames_disagreement)
        writer.writeheader()
        writer.writerows(rows_disagreement)


if __name__ == '__main__':

    timespans = [(10426, 440818), (1252816, 1358482), (5004410, 5421143)]

    # Original annotations
    classes = {'question_form': ['YN', 'WH', 'ITJ', 'Other'], 'question_multiple': ['MULT','PART'],
               'question_disjonction': ['DQI', 'DQE'], 'question_impliedform': ['YN','WH','Other'],
               'question_feature': ['TMP', 'LOC', 'AG', 'TH', 'OW', 'RE', 'CH'], 'question_clarification': ['CLR'],
               'question_quoted': ['QS', 'RS'], 'question_phatic': ['PHA'], 
               'answer_type': ['CORE', 'HLP', 'CMM', 'Other'],'answer_quoted': ['QS', 'RS'],
               'answer_form': ['YES','NO','FEAT','UNK','UNC','BOTH','NEITHER','DP','AVOID','PERF','Other'],
               'answer_feature': ['TMP', 'LOC', 'AG', 'TH', 'OW', 'RE', 'CH'], 'answer_phatic': ['PHA'],
               'answer_mismatch': ['MM_Form','MM_Feat','MM_Prag','MM_Other'],
               'answer_impliedform': ['YN','YES','NO'], 'answer_multiple': ['MULT', 'PART']
               }
    
    #Change the name at the end of the path to correspond to your file !
    all_annotators = ['../preprocessing/output/amy_axel_2020.json', '../preprocessing/output/amy_alena_2020.json',
                      '../preprocessing/output/amy_GOLD_2020.json']
    path_output = 'output_metrics/after_clean_calculation/metrics/'


#    #if you want the average :
#    annotations_task, utterances, times = calculate_annotation_task(all_annotators, timespans)
#    write_all_csv(annotations_task, utterances, times, all_annotators, path_output + '1all/')
#    calculate_metrics(annotations_task, path_output + '1all/metrics.json')
#    print(calculate_kappa_sklearn(annotations_task, classes, path_output + '1all/kappa_sklearn.json'))


    #CHANGE : the name at the end, before /metrics.json (should be the annotator1 and annotator2)
    #Remind : Annotator1 is the second of all_annotators you write above
    print(all_annotators[1])
    axel_alena = [all_annotators[0], all_annotators[1]]
    annotations_task_1, utterances_1, times_1 = calculate_annotation_task(axel_alena, timespans)
    write_all_csv(annotations_task_1, utterances_1, times_1, axel_alena, path_output + '2axel_alena/')
    calculate_metrics(annotations_task_1, path_output + '2axel_alena/metrics.json')

    #CHANGE : same with annotator0 and annotator1
    axel_andrea = [all_annotators[0], all_annotators[2]]
    annotations_task_7, utterances_7, times_7 = calculate_annotation_task(axel_andrea, timespans)
    write_all_csv(annotations_task_7, utterances_7, times_7, axel_andrea, path_output + '3axel_GOLD/')
    calculate_metrics(annotations_task_7, path_output + '3axel_GOLD/metrics.json')
    
    axel_aria = [all_annotators[1], all_annotators[2]]
    annotations_task_8, utterances_8, times_8 = calculate_annotation_task(axel_aria, timespans)
    write_all_csv(annotations_task_8, utterances_8, times_8, axel_aria, path_output + '4GOLD_Alena/')
    calculate_metrics(annotations_task_8, path_output + '4GOLD_alena/metrics.json')
    
    

