Output created: 05/13/20 12:11:33

Number of files involved: 2
Number of selected tiers: 32
Number of pairs of tiers in the comparison: 29
Required minimal overlap percentage: 100%

Results per value	kappa	kappa_max	raw agreement

"empty"	0.0000	0.0000	0.9984
AG	0.7676	0.9225	0.9968
AVOID	0.0000	0.0000	0.9995
CH	0.8680	0.8680	0.9968
CLR	0	0.1658	0.9935
CMM	0.8586	0.9456	0.9859
CORE	0.9154	0.9577	0.9784
DP	1.0000	1.0000	1.0000
DQE	0.9409	0.9409	0.9995
DQI	0.0000	0.0000	0.9995
FEAT	0.8717	0.9572	0.9935
HLP	0.5948	0.9301	0.9843
ITJ	1.0000	1.0000	1.0000
LOC	1.0000	1.0000	1.0000
MM_Form	0.0000	0.0000	0.9995
MULT	0.7265	0.7265	0.9984
NO	0.8813	0.9909	0.9930
Other	0.0000	0.0000	0.9946
PHA	0.8614	0.9176	0.9600
QS	0.9463	0.9731	0.9978
RDC	0.6419	0.7521	0.9859
RE	0.6141	0.7685	0.9973
TH	0.8749	1.0000	0.9903
TMP	0.7390	0.7390	0.9962
UNC	0.7265	0.9088	0.9984
UNK	0.8323	0.8323	0.9978
WH	0.9542	0.9771	0.9957
YES	0.8967	0.9343	0.9822
YN	0.9052	0.9967	0.9686


Global results (incl. unlinked/unmatched annotations):
kappa_ipf	kappa_max	raw agreement
0.8191	0.9234	0.8400

Global results (excl. unlinked/unmatched annotations):
kappa (excl.)	kappa_max (excl.)	raw agreement (excl.)
0.9254	0.9585	0.9350

Global Agreement Matrix:
First annotator in the rows, second annotator in the columns

"empty"	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
AG	0	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	0	0	
AVOID	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
CH	0	0	0	20	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	2	
CLR	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	
CMM	0	0	0	0	0	84	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	
CORE	0	0	0	0	0	10	258	0	0	0	0	16	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	
DP	0	0	0	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
DQE	0	0	0	0	0	0	0	0	8	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
DQI	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
FEAT	0	0	0	0	0	0	0	0	0	0	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	
HLP	0	0	0	0	0	8	4	0	0	0	0	22	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
ITJ	0	0	0	0	0	0	0	0	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
LOC	0	0	0	0	0	0	0	0	0	0	0	0	0	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
MM_Form	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
MULT	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
NO	0	0	0	0	0	0	0	0	0	0	2	0	0	0	0	0	50	0	0	0	0	0	0	0	0	2	0	0	0	3	
Other	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
PHA	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	286	0	0	0	0	0	0	0	0	0	0	15	
QS	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	0	0	0	0	0	0	0	0	0	3	
RDC	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	0	0	24	0	0	0	0	0	2	0	0	0	
RE	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	0	0	1	
TH	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	66	0	0	0	0	0	0	5	
TMP	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	0	0	
UNC	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	2	0	0	0	0	
UNK	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	
WH	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	0	0	0	0	0	0	0	0	88	0	0	0	
YES	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	160	8	13	
YN	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	22	0	0	0	0	0	0	0	358	8	
Unmatched	3	2	1	0	11	0	0	0	1	1	2	1	0	0	1	3	4	2	59	1	0	0	5	3	1	0	0	6	20	0	


Global per value agreement table:
"empty"
0	3
0	1847

AG
10	2
4	1834

AVOID
0	1
0	1849

CH
20	0
6	1824

CLR
0	11
1	1838

CMM
84	18
8	1740

CORE
258	10
30	1552

DP
2	0
0	1848

DQE
8	1
0	1841

DQI
0	1
0	1849

FEAT
42	8
4	1796

HLP
22	17
12	1799

ITJ
2	0
0	1848

LOC
6	0
0	1844

MM_Form
0	1
0	1849

MULT
4	3
0	1843

NO
50	6
7	1787

Other
0	10
0	1840

PHA
286	59
15	1490

QS
36	1
3	1810

RDC
24	22
4	1800

RE
4	4
1	1841

TH
66	9
9	1766

TMP
10	7
0	1833

UNC
4	1
2	1843

UNK
10	4
0	1836

WH
88	2
6	1754

YES
160	6
27	1657

YN
358	28
30	1434


End of global results.
################################################

Results per tier combination: kappa,	kappa_max,	raw agreement
File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_A_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_A_Feature
AG	0.5234	0.8411	0.9118
CH	1.0000	1.0000	1.0000
LOC	1.0000	1.0000	1.0000
RE	0.6383	0.6383	0.9412
TH	0.6471	1.0000	0.8235
TMP	0.7848	0.7848	0.9706

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_A_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_A_Form
DP	1.0000	1.0000	1.0000
FEAT	0.8878	0.9252	0.9653
NO	0.9233	0.9617	0.9769
UNC	0.6614	0.6614	0.9884
UNK	0.6561	0.6561	0.9769
YES	0.8617	0.8617	0.9306
YN	0.0000	0.0000	0.9942

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_A_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_A_Implied_Form
NO	0.6106	0.8053	0.8182
YES	0.2667	1.0000	0.6364

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_A_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_A_Phatic
PHA	0	0.3590	0.8000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_A_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_A_Quoted
QS	0.0000	0.0000	0.5000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_A_Type
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_A_Type
CMM	0.8162	0.9081	0.9360
CORE	0.7670	0.8788	0.9000
HLP	0.5489	0.8673	0.9320

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_Clarification
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_Clarification
CLR	0	0.0541	0.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_DISJ
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_DISJ
DQE	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_Feature
AG	1.0000	1.0000	1.0000
CH	0.7750	0.7750	0.9111
LOC	1.0000	1.0000	1.0000
RE	1.0000	1.0000	1.0000
TH	0.9541	0.9541	0.9778
TMP	0.4643	0.4643	0.9111

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_Form
Other	0.0000	0.0000	0.9908
RDC	0.4742	0.6494	0.9447
WH	0.9448	1.0000	0.9816
YN	0.8731	0.8731	0.9493

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_Implied_Form
YES	0.0000	0.0000	0.9333
YN	0.1509	0.1509	0.6000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_Multiple
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_Multiple
MULT	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_Phatic
PHA	0	0.2347	0.8193

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 1_Q_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 1_Q_Quoted
QS	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_A_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_A_Feature
CH	0.5946	0.5946	0.8667
TH	0.3243	0.8649	0.6667
TMP	1.0000	1.0000	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_A_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_A_Form
"empty"	0.0000	0.0000	0.9912
AVOID	0.0000	0.0000	0.9912
FEAT	0.7697	1.0000	0.9474
NO	0.8742	0.8742	0.9737
Other	0.0000	0.0000	0.9912
UNC	0.7957	0.7957	0.9912
UNK	1.0000	1.0000	1.0000
YES	0.8382	0.8742	0.9211
YN	0.0000	0.0000	0.9298

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_A_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_A_Implied_Form
"empty"	0.0000	0.0000	0.9474
NO	0.6122	0.6122	0.8947
Other	0.0000	0.0000	0.9474
TH	0.0000	0.0000	0.9474
TMP	0.0000	0.0000	0.9474
YES	0.6919	0.6919	0.8421

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_A_Mismatch
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_A_Mismatch
MM_Form	0.0000	0.0000	0.5000
PHA	0.0000	0.0000	0.5000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_A_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_A_Phatic
"empty"	0.0000	0.0000	0.9787
PHA	0	0.4749	0.6596

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_A_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_A_Quoted
QS	0.0000	0.0000	0.6667

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_A_Type
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_A_Type
CMM	0.8378	0.9676	0.9394
CORE	0.8032	0.9082	0.9091
HLP	0.5847	1.0000	0.9273

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_Clarification
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_Clarification
CLR	0.0000	0.0000	0.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_DISJ
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_DISJ
DQE	0.6000	0.6000	0.8750
DQI	0.0000	0.0000	0.8750

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_Feature
AG	0.5399	0.8466	0.9400
CH	1.0000	1.0000	1.0000
LOC	1.0000	1.0000	1.0000
RE	0	0.6575	0.9400
TH	0.7803	0.9561	0.9000
TMP	0.8780	0.8780	0.9800

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_Form
ITJ	1.0000	1.0000	1.0000
Other	0.0000	0.0000	0.9779
RDC	0.6928	0.7806	0.9483
WH	0.9477	0.9477	0.9852
YN	0.7989	0.8689	0.9151

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_Implied_Form
YN	0.0000	0.0000	0.6897

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_Multiple
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_Multiple
MULT	0.0000	0.0000	0.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_Phatic
PHA	0	0.4201	0.8308

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 1: 2_Q_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_alena.eaf Tier 2: 2_Q_Quoted
QS	0.0000	0.0000	0.9231

