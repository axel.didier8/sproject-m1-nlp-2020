Output created: 05/13/20 12:13:15

Number of files involved: 2
Number of selected tiers: 32
Number of pairs of tiers in the comparison: 28
Required minimal overlap percentage: 100%

Results per value	kappa	kappa_max	raw agreement

AG	0.7676	0.9225	0.9968
AVOID	0.0000	0.0000	0.9995
CH	0.9404	0.9404	0.9984
CLR	0.4435	0.4435	0.9973
CMM	0.8943	0.9366	0.9892
CORE	0.9287	0.9539	0.9817
DP	1.0000	1.0000	1.0000
DQE	1.0000	1.0000	1.0000
FEAT	0.9491	0.9695	0.9973
HLP	0.7544	0.8578	0.9898
ITJ	1.0000	1.0000	1.0000
LOC	1.0000	1.0000	1.0000
MM_Form	0.0000	0.0000	0.9995
MULT	1.0000	1.0000	1.0000
NO	0.8771	0.9905	0.9930
PHA	0.8894	0.9076	0.9672
QS	0.9880	0.9880	0.9995
RDC	0.6765	0.6765	0.9860
RE	0.7490	0.7490	0.9978
TH	0.9416	0.9935	0.9952
TMP	0.8323	0.8323	0.9978
UNC	1.0000	1.0000	1.0000
UNK	0.9086	0.9086	0.9989
WH	1.0000	1.0000	1.0000
YES	0.9276	0.9578	0.9871
YN	0.9137	0.9886	0.9715


Global results (incl. unlinked/unmatched annotations):
kappa_ipf	kappa_max	raw agreement
0.8734	0.9253	0.8885

Global results (excl. unlinked/unmatched annotations):
kappa (excl.)	kappa_max (excl.)	raw agreement (excl.)
0.9445	0.9604	0.9516

Global Agreement Matrix:
First annotator in the rows, second annotator in the columns

AG	10	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	
AVOID	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	
CH	0	0	24	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
CLR	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	
CMM	0	0	0	0	90	14	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
CORE	0	0	0	0	4	264	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
DP	0	0	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
DQE	0	0	0	0	0	0	0	8	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
FEAT	0	0	0	0	0	0	0	0	48	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	
HLP	0	0	0	0	0	14	0	0	0	30	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	
ITJ	0	0	0	0	0	0	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
LOC	0	0	0	0	0	0	0	0	0	0	0	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
MM_Form	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	
MULT	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	0	0	0	0	0	0	0	0	
NO	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	0	0	0	0	0	0	0	0	0	2	0	4	
PHA	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	306	0	0	0	0	0	0	0	0	0	0	56	
QS	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	42	0	0	0	0	0	0	0	0	0	1	
RDC	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	28	0	0	0	0	0	0	0	26	0	
RE	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	4	0	0	0	0	0	0	0	
TH	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	76	0	0	0	0	0	0	1	
TMP	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	0	0	2	
UNC	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	0	0	0	0	0	
UNK	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	0	0	0	0	0	0	0	10	0	0	0	0	
WH	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	94	0	0	0	
YES	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	172	0	5	
YN	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	362	19	
Unmatched	0	0	1	0	0	0	0	0	1	0	0	0	0	0	5	5	0	0	0	0	0	0	0	0	9	4	0	


Global per value agreement table:
AG
10	4
2	1841

AVOID
0	0
1	1856

CH
24	3
0	1830

CLR
2	0
5	1850

CMM
90	4
16	1747

CORE
264	28
6	1559

DP
2	0
0	1855

DQE
8	0
0	1849

FEAT
48	1
4	1804

HLP
30	4
15	1808

ITJ
2	0
0	1855

LOC
6	0
0	1851

MM_Form
0	0
1	1856

MULT
4	0
0	1853

NO
48	7
6	1796

PHA
306	5
56	1490

QS
42	0
1	1814

RDC
28	0
26	1803

RE
6	0
4	1847

TH
76	4
5	1772

TMP
10	0
4	1843

UNC
6	0
0	1851

UNK
10	0
2	1845

WH
94	0
0	1763

YES
172	19
5	1661

YN
362	30
23	1442


End of global results.
################################################

Results per tier combination: kappa,	kappa_max,	raw agreement
File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_A_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_A_Feature
AG	0.5234	0.8411	0.9118
CH	1.0000	1.0000	1.0000
LOC	1.0000	1.0000	1.0000
RE	0.6383	0.6383	0.9412
TH	0.7639	1.0000	0.8824
TMP	0.7848	0.7848	0.9706

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_A_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_A_Form
DP	1.0000	1.0000	1.0000
FEAT	0.9266	0.9266	0.9766
NO	0.9003	0.9402	0.9708
UNC	1.0000	1.0000	1.0000
UNK	0.7942	0.7942	0.9883
YES	0.8600	0.8600	0.9298

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_A_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_A_Implied_Form
NO	0.6106	0.8053	0.8182
YES	0.4545	1.0000	0.7273

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_A_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_A_Phatic
PHA	0	0.2228	0.8400

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_A_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_A_Quoted
QS	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_A_Type
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_A_Type
CMM	0.8456	0.8456	0.9442
CORE	0.7994	0.8358	0.9124
HLP	0.6552	0.8674	0.9482

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_Clarification
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_Clarification
CLR	0.0000	0.0000	0.5000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_DISJ
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_DISJ
DQE	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_Feature
AG	1.0000	1.0000	1.0000
CH	0.8930	0.8930	0.9565
LOC	1.0000	1.0000	1.0000
RE	1.0000	1.0000	1.0000
TH	1.0000	1.0000	1.0000
TMP	0.6462	0.6462	0.9565

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_Form
RDC	0.5477	0.5477	0.9450
WH	1.0000	1.0000	1.0000
YN	0.8625	0.8625	0.9450

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_Implied_Form
YES	0.0000	0.0000	0.9333
YN	0.1509	0.1509	0.6000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_Multiple
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_Multiple
MULT	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_Phatic
PHA	0.0000	0.0000	0.8571

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 1_Q_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 1_Q_Quoted
QS	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_A_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_A_Feature
CH	0.8496	0.8496	0.9412
TH	1.0000	1.0000	1.0000
TMP	1.0000	1.0000	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_A_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_A_Form
AVOID	0.0000	0.0000	0.9910
FEAT	0.9644	0.9644	0.9910
NO	0.9129	0.9129	0.9820
UNC	1.0000	1.0000	1.0000
UNK	1.0000	1.0000	1.0000
YES	0.9048	0.9429	0.9550
YN	0.0000	0.0000	0.9640

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_A_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_A_Implied_Form
NO	0.6087	0.6087	0.8889
YES	1.0000	1.0000	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_A_Mismatch
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_A_Mismatch
MM_Form	0.0000	0.0000	0.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_A_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_A_Phatic
PHA	0.0000	0.0000	0.7170

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_A_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_A_Quoted
QS	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_A_Type
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_A_Type
CMM	0.9036	0.9679	0.9647
CORE	0.8455	0.9485	0.9294
HLP	0.8228	0.8228	0.9647

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_Q_Clarification
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_Q_Clarification
CLR	0.0000	0.0000	0.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_Q_DISJ
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_Q_DISJ
DQE	NaN	NaN	1.0000

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_Q_Feature
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_Q_Feature
AG	0.5405	0.8468	0.9412
CH	1.0000	1.0000	1.0000
LOC	1.0000	1.0000	1.0000
RE	0.6483	0.6483	0.9608
TH	0.7880	0.9576	0.9020
TMP	0.8783	0.8783	0.9804

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_Q_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_Q_Form
ITJ	1.0000	1.0000	1.0000
RDC	0.7145	0.7145	0.9489
WH	1.0000	1.0000	1.0000
YN	0.8078	0.8777	0.9197

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_Q_Implied_Form
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_Q_Implied_Form
YN	0.0000	0.0000	0.6897

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_Q_Phatic
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_Q_Phatic
PHA	0	0.3067	0.8615

File 1: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_GOLD.eaf Tier 1: 2_Q_Quoted
File 2: file:///Users/alena/Desktop/Project - Coffee or Tea/amy_2020_axel.eaf Tier 2: 2_Q_Quoted
QS	0.0000	0.0000	0.9231

